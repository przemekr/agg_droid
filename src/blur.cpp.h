#include "app_support.h"
#include "agg_blur.h"

class Blur: public AppView
{
    agg::rbox_ctrl<agg::rgba8>    m_method;
    agg::slider_ctrl<agg::rgba8>  m_radius;
    agg::polygon_ctrl<agg::rgba8> m_shadow_ctrl;
    agg::cbox_ctrl<agg::rgba8>    m_channel_r;
    agg::cbox_ctrl<agg::rgba8>    m_channel_g;
    agg::cbox_ctrl<agg::rgba8>    m_channel_b;

    agg::path_storage             m_path;
    typedef agg::conv_curve<agg::path_storage> shape_type;
    shape_type                    m_shape;

    agg::rasterizer_scanline_aa<> m_ras;
    agg::scanline_p8              m_sl;
    agg::rendering_buffer         m_rbuf2;

    agg::stack_blur    <agg::rgba8, agg::stack_blur_calc_rgb<> >     m_stack_blur;
    agg::recursive_blur<agg::rgba8, agg::recursive_blur_calc_rgb<> > m_recursive_blur;

    agg::rect_d m_shape_bounds;


public:
    Blur(App& app);
    virtual void on_draw();
};
