#include "app_support.h"

class Lion_outline: public AppView
{
    agg::slider_ctrl<agg::rgba8> m_width_slider;
    agg::cbox_ctrl<agg::rgba8>   m_scanline;

public:
    Lion_outline(App& app);
    virtual void on_draw();
    void transform(double width, double height, double x, double y);
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
};
