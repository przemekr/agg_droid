#include "app_support.h"
#include "ctrl/agg_spline_ctrl.h"

class Gradients: public AppView
{
    agg::gamma_ctrl<agg::rgba8>  m_profile;
    agg::spline_ctrl<agg::rgba8> m_spline_r;
    agg::spline_ctrl<agg::rgba8> m_spline_g;
    agg::spline_ctrl<agg::rgba8> m_spline_b;
    agg::spline_ctrl<agg::rgba8> m_spline_a;
    agg::rbox_ctrl<agg::rgba8>   m_rbox;

    double m_pdx;
    double m_pdy;
    double m_center_x;
    double m_center_y;
    double m_scale;
    double m_prev_scale;
    double m_angle;
    double m_prev_angle;
    double m_scale_x;
    double m_prev_scale_x;
    double m_scale_y;
    double m_prev_scale_y;
    bool m_mouse_move;

public:
    virtual ~Gradients();
    Gradients(App& app);
    virtual void on_draw();
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
    virtual void on_key(int x, int y, unsigned key, unsigned flags);
};
