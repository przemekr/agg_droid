#include "app_support.h"
#include "agg_gamma_lut.h"
#include "agg_gradient_lut.h"

class Gradient_focal: public AppView
{
    typedef agg::gamma_lut<agg::int8u, agg::int8u> gamma_lut_type;
    typedef agg::gradient_lut<agg::color_interpolator<agg::rgba8>, 1024> color_func_type;
    typedef agg::span_allocator<color_type> span_allocator_type;
    
    agg::slider_ctrl<agg::rgba8>    m_gamma;
    agg::scanline_u8                m_scanline;
    agg::rasterizer_scanline_aa<>   m_rasterizer;
    span_allocator_type             m_alloc;
    color_func_type                 m_gradient_lut;
    gamma_lut_type                  m_gamma_lut;
    double m_mouse_x, m_mouse_y;
    double m_old_gamma;


public:
    Gradient_focal(App& app);
    virtual void on_init();
    void build_gradient_lut();
    virtual void on_draw();
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
};
