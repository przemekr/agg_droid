class Polymorphic_renderer: public AppView
{
    double m_x[3];
    double m_y[3];

public:
    Polymorphic_renderer(App& app) :
        AppView(app):
    {
        m_x[0] = 100; m_y[0] = 60;
        m_x[1] = 369; m_y[1] = 170;
        m_x[2] = 143; m_y[2] = 310;
    }

    virtual void on_draw()
    {
        agg::path_storage path;
        path.move_to(m_x[0], m_y[0]);
        path.line_to(m_x[1], m_y[1]);
        path.line_to(m_x[2], m_y[2]);
        path.close_polygon();

        agg::polymorphic_renderer_solid_rgba8_base* ren = 0;

        //-- Polymorphic renderer class factory
        switch(pix_fmt)
        {
        case agg::pix_format_rgb555 : ren = new agg::polymorphic_renderer_solid_rgba8_adaptor<agg::pixfmt_rgb555>(rbuf_window()); break;
        case agg::pix_format_rgb565 : ren = new agg::polymorphic_renderer_solid_rgba8_adaptor<agg::pixfmt_rgb565>(rbuf_window()); break;
        case agg::pix_format_rgb24  : ren = new agg::polymorphic_renderer_solid_rgba8_adaptor<agg::pixfmt_rgb24 >(rbuf_window()); break;
        case agg::pix_format_bgr24  : ren = new agg::polymorphic_renderer_solid_rgba8_adaptor<agg::pixfmt_bgr24 >(rbuf_window()); break;
        case agg::pix_format_rgba32 : ren = new agg::polymorphic_renderer_solid_rgba8_adaptor<agg::pixfmt_rgba32>(rbuf_window()); break;
        case agg::pix_format_argb32 : ren = new agg::polymorphic_renderer_solid_rgba8_adaptor<agg::pixfmt_argb32>(rbuf_window()); break;
        case agg::pix_format_abgr32 : ren = new agg::polymorphic_renderer_solid_rgba8_adaptor<agg::pixfmt_abgr32>(rbuf_window()); break;
        case agg::pix_format_bgra32 : ren = new agg::polymorphic_renderer_solid_rgba8_adaptor<agg::pixfmt_bgra32>(rbuf_window()); break;
        }

        agg::rasterizer_scanline_aa<> ras;
        agg::scanline_p8 sl;
        if(ren) 
        {
            ren->clear(agg::rgba8(255, 255, 255));
            ren->color(agg::rgba8(80, 30, 20));
            ras.add_path(path);
            agg::render_scanlines(ras, sl, *ren);
        }
        delete ren;
    }

};
