#include "app_support.h"

class Multi_clip: public AppView
{
    agg::slider_ctrl<agg::rgba8> m_num_cb;

public:
    Multi_clip(App& app);
    virtual void on_draw();
    void transform(double width, double height, double x, double y);
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
};
