#include "app_support.h"

class Simple_blur: public AppView
{
    double m_cx;
    double m_cy;

public:
    virtual ~Simple_blur();
    Simple_blur(App& app);
    virtual void on_resize(int cx, int cy);
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
};
