#include "app_support.h"

class Distortions: public AppView
{
    agg::slider_ctrl<agg::rgba8> m_angle;
    agg::slider_ctrl<agg::rgba8> m_scale;
    agg::slider_ctrl<agg::rgba8> m_amplitude;
    agg::slider_ctrl<agg::rgba8> m_period;
    agg::rbox_ctrl<agg::rgba8>   m_distortion;

    double m_center_x;
    double m_center_y;
    double m_phase;

    typedef agg::pod_auto_array<agg::rgba8, 256> color_array_type;
    color_array_type m_gradient_colors;

public:
    Distortions(App& app);
     ~Distortions();
     void on_init();
     void on_draw();
     void on_mouse_button_down(int x, int y, unsigned flags);
     void on_mouse_move(int x, int y, unsigned flags);
     void on_idle();
};
