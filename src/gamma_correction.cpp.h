#include "app_support.h"

class Gamma_correction: public AppView
{
    agg::slider_ctrl<agg::rgba8> m_thickness;
    agg::slider_ctrl<agg::rgba8> m_gamma;
    agg::slider_ctrl<agg::rgba8> m_contrast;
    double m_rx;
    double m_ry;

public:
    Gamma_correction(App& app);
    virtual void on_init();
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
};
