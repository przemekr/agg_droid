#include "app_support.h"

class Rasterizer_compound: public AppView
{
    agg::slider_ctrl<agg::rgba8> m_width;
    agg::slider_ctrl<agg::rgba8> m_alpha1;
    agg::slider_ctrl<agg::rgba8> m_alpha2;
    agg::slider_ctrl<agg::rgba8> m_alpha3;
    agg::slider_ctrl<agg::rgba8> m_alpha4;
    agg::cbox_ctrl<agg::rgba8>   m_invert_order;
    agg::path_storage            m_path;

public:
    Rasterizer_compound(App& app);
    void compose_path();
    virtual void on_draw();
};
