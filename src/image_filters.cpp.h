#include "app_support.h"

class Image_filters: public AppView
{
    typedef agg::renderer_base<pixfmt_type> renderer_base;
    typedef agg::renderer_base<pixfmt_pre> renderer_base_pre;

    agg::slider_ctrl<agg::rgba> m_radius;
    agg::slider_ctrl<agg::rgba> m_step;
    agg::rbox_ctrl<agg::rgba>   m_filters;
    agg::cbox_ctrl<agg::rgba>   m_normalize;
    agg::cbox_ctrl<agg::rgba>   m_run;
    agg::cbox_ctrl<agg::rgba>   m_single_step;
    agg::cbox_ctrl<agg::rgba>   m_refresh;

    double  m_cur_angle;
    int     m_cur_filter;
    int     m_num_steps;
    double  m_num_pix;
    double  m_time1;
    double  m_time2;

public:
    Image_filters(App& app);
    virtual ~Image_filters();
    virtual void on_draw();
    void transform_image(double angle);
    void on_ctrl_change();
    void on_idle();
};
