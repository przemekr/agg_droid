#include "app_support.h"

class Line_patterns_clip: public AppView
{
    agg::rgba8 m_ctrl_color;
    agg::polygon_ctrl<agg::rgba8> m_line1;
    agg::slider_ctrl<agg::rgba8>  m_scale_x;
    agg::slider_ctrl<agg::rgba8>  m_start_x;
    agg::trans_affine             m_scale;

public:
    Line_patterns_clip(App& app);
    template<class Rasterizer, class Renderer>
    void draw_polyline(Rasterizer& ras, 
                       Renderer& ren, 
                       const double* polyline, 
                       int num_points);
    virtual void on_draw();
    virtual void on_key(int x, int y, unsigned key, unsigned flags);
    virtual void on_ctrl_change();
};
