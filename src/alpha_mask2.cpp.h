#include "app_support.h"
#include "agg_alpha_mask_u8.h"
#include "agg_pixfmt_amask_adaptor.h"

class Alpha_mask2: public AppView
{
    agg::slider_ctrl<agg::rgba> m_num_cb;

    typedef agg::amask_no_clip_gray8 alpha_mask_type;
    //typedef agg::alpha_mask_gray8 alpha_mask_type;

    unsigned char* m_alpha_buf;
    agg::rendering_buffer m_alpha_mask_rbuf;
    alpha_mask_type m_alpha_mask;

    double m_slider_value;


public:
    ~Alpha_mask2();

    Alpha_mask2(App& app);
    void generate_alpha_mask(int cx, int cy);
    virtual void on_resize(int cx, int cy);
    virtual void on_draw();
    void transform(double width, double height, double x, double y);
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
};
