#include "app_support.h"



struct scatter_point
{
   double     x;
   double     y;
   double     z;
   agg::rgba  color;
};

class Circles: public AppView
{
    unsigned       m_num_points;
    scatter_point* m_points;

    agg::scale_ctrl<agg::rgba8>  m_scale_ctrl_z;
    agg::slider_ctrl<agg::rgba8> m_slider_ctrl_sel;
    agg::slider_ctrl<agg::rgba8> m_slider_ctrl_size;

    agg::bspline m_spline_r;
    agg::bspline m_spline_g;
    agg::bspline m_spline_b;

public:
    virtual ~Circles();
    Circles(App&);
    void generate();
    virtual void on_init();
    virtual void on_draw();
    virtual void on_idle();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
};
