#include <math.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "agg_rendering_buffer.h"
#include "agg_trans_viewport.h"
#include "agg_path_storage.h"
#include "agg_conv_transform.h"
#include "agg_conv_curve.h"
#include "agg_conv_stroke.h"
#include "agg_gsv_text.h"
#include "agg_scanline_u.h"
#include "agg_scanline_bin.h"
#include "agg_renderer_scanline.h"
#include "agg_rasterizer_scanline_aa.h"
#include "agg_rasterizer_compound_aa.h"
#include "agg_span_allocator.h"
#include "agg_gamma_lut.h"
#include "agg_pixfmt_rgba.h"
#include "agg_bounding_rect.h"
#include "agg_color_gray.h"
#include "platform/agg_platform_support.h"
#include "flash_rasterizer.cpp.h"



Flash_rasterizer::Flash_rasterizer(App& app) :
   AppView(app),
   m_point_idx(-1),
   m_hit_x(-1),
   m_hit_y(-1),
   next (20, 40,  130, 60,  "Next ", !flip_y),
   left (20, 70,  130, 90,  "Left ", !flip_y),
   right(20, 100, 130, 120, "Right", !flip_y)
{
   next.background_color(red);
   left.background_color(red);
   right.background_color(red);
   add_ctrl(next);
   add_ctrl(left);
   add_ctrl(right);
   m_gamma.gamma(2.0);

   for(unsigned i = 0; i < 100; i++)
   {
      m_colors[i] = agg::rgba8(
            (rand() & 0xFF), 
            (rand() & 0xFF), 
            (rand() & 0xFF), 
            230);

      m_colors[i].apply_gamma_dir(m_gamma);
      m_colors[i].premultiply();
   }
}



bool Flash_rasterizer::open(const char* fname)
{
   return m_shape.open(app.full_file_name(fname));
}

void Flash_rasterizer::read_next()
{
   m_shape.read_next();
   m_shape.scale(width(), height());
}

void Flash_rasterizer::on_draw()
{
   typedef agg::renderer_base<pixfmt_type> renderer_base;
   typedef agg::renderer_scanline_aa_solid<renderer_base> renderer_scanline;
   typedef agg::scanline_u8 scanline;

   pixfmt_type pixf(rbuf_window());
   renderer_base ren_base(pixf);
   ren_base.clear(agg::rgba(1.0, 1.0, 0.95));
   renderer_scanline ren(ren_base);

   unsigned i;
   unsigned w = unsigned(width());
   m_gradient.resize(w);
   agg::rgba8 c1(255, 0, 0, 180);
   agg::rgba8 c2(0, 0, 255, 180);
   for(i = 0; i < w; i++)
   {
      m_gradient[i] = c1.gradient(c2, i / width());
      m_gradient[i].premultiply();
   }

   agg::rasterizer_scanline_aa<agg::rasterizer_sl_clip_dbl> ras;
   agg::rasterizer_compound_aa<agg::rasterizer_sl_clip_dbl> rasc;
   agg::scanline_u8 sl;
   agg::scanline_bin sl_bin;
   agg::conv_transform<agg::compound_shape> shape(m_shape, m_scale);
   agg::conv_stroke<agg::conv_transform<agg::compound_shape> > stroke(shape);

   agg::test_styles style_handler(m_colors, m_gradient.data());
   agg::span_allocator<agg::rgba8> alloc;

   m_shape.approximation_scale(m_scale.scale());

   // Fill shape
   //----------------------
   rasc.clip_box(0, 0, width(), height());
   rasc.reset();
   //rasc.filling_rule(agg::fill_even_odd);
   start_timer();
   for(i = 0; i < m_shape.paths(); i++)
   {

      if(m_shape.style(i).left_fill >= 0 || 
            m_shape.style(i).right_fill >= 0)
      {
         rasc.styles(m_shape.style(i).left_fill, 
               m_shape.style(i).right_fill);
         rasc.add_path(shape, m_shape.style(i).path_id);
      }
   }
   agg::render_scanlines_compound(rasc, sl, sl_bin, ren_base, alloc, style_handler);
   double tfill = elapsed_time();

   // Hit-test test
   bool draw_strokes = true;
   if(m_hit_x >= 0 && m_hit_y >= 0)
   {
      if(rasc.hit_test(m_hit_x, m_hit_y))
      {
         draw_strokes = false;
      }
   }

   // Draw strokes
   //----------------------
   start_timer();
   if(draw_strokes)
   {
      ras.clip_box(0, 0, width(), height());
      stroke.width(sqrt(m_scale.scale()));
      stroke.line_join(agg::round_join);
      stroke.line_cap(agg::round_cap);
      for(i = 0; i < m_shape.paths(); i++)
      {
         ras.reset();
         if(m_shape.style(i).line >= 0)
         {
            ras.add_path(stroke, m_shape.style(i).path_id);
            ren.color(agg::rgba8(0,0,0, 128));
            agg::render_scanlines(ras, sl, ren);
         }
      }
   }
   double tstroke = elapsed_time();


   char buf[256]; 
   agg::gsv_text t;
   t.size(8.0);
   t.flip(false);

   agg::conv_stroke<agg::gsv_text> ts(t);
   ts.width(1.6);
   ts.line_cap(agg::round_cap);

   sprintf(buf, "Fill=%.2fms (%dFPS) Stroke=%.2fms (%dFPS) Total=%.2fms (%dFPS)\n\n"
         "Space: Next Shape\n\n"
         "+/- : ZoomIn/ZoomOut (with respect to the mouse pointer)",
         tfill, int(1000.0 / tfill),
         tstroke, int(1000.0 / tstroke),
         tfill+tstroke, int(1000.0 / (tfill+tstroke)));

   t.start_point(10.0, 20.0);
   t.text(buf);

   ras.add_path(ts);
   ren.color(agg::rgba(0,0,0));
   agg::render_scanlines(ras, sl, ren);

   if(m_gamma.gamma() != 1.0)
   {
      pixf.apply_gamma_inv(m_gamma);
   }

   agg::render_ctrl(ras, sl, ren_base, next);
   agg::render_ctrl(ras, sl, ren_base, left);
   agg::render_ctrl(ras, sl, ren_base, right);
}


void Flash_rasterizer::on_key(int x, int y, unsigned key, unsigned flags)
{
   if(key == ' ')
   {
      m_shape.read_next();
      m_shape.scale(width(), height());
      force_redraw();
   }

   if(key == '+' || key == agg::key_kp_plus)
   {
      m_scale *= agg::trans_affine_translation(-x, -y);
      m_scale *= agg::trans_affine_scaling(1.1);
      m_scale *= agg::trans_affine_translation(x, y);
      force_redraw();
   }

   if(key == '-' || key == agg::key_kp_minus)
   {
      m_scale *= agg::trans_affine_translation(-x, -y);
      m_scale *= agg::trans_affine_scaling(1/1.1);
      m_scale *= agg::trans_affine_translation(x, y);
      force_redraw();
   }

   if(key == agg::key_left)
   {
      m_scale *= agg::trans_affine_translation(-x, -y);
      m_scale *= agg::trans_affine_rotation(-agg::pi / 20.0);
      m_scale *= agg::trans_affine_translation(x, y);
      force_redraw();
   }

   if(key == agg::key_right)
   {
      m_scale *= agg::trans_affine_translation(-x, -y);
      m_scale *= agg::trans_affine_rotation(agg::pi / 20.0);
      m_scale *= agg::trans_affine_translation(x, y);
      force_redraw();
   }
}

void Flash_rasterizer::on_ctrl_change()
{
   if (next.status())
   {
      next.status(false);
      on_key(0, 0, ' ', 0);
   }
   if (left.status())
   {
      left.status(false);
      on_key(width()/2, height()/2, agg::key_left, 0);
   }
   if (right.status())
   {
      right.status(false);
      on_key(width()/2, height()/2, agg::key_right, 0);
   }

}

void Flash_rasterizer::on_mouse_move(int x, int y, unsigned flags) 
{
   if((flags & 3) == 0)
   {
      on_mouse_button_up(x, y, flags);
   }
   else
   {
      if(m_point_idx >= 0)
      {
         double xd = x;
         double yd = y;
         m_scale.inverse_transform(&xd, &yd);
         m_shape.modify_vertex(m_point_idx, xd, yd);
         force_redraw();
      }
   }
}

void Flash_rasterizer::on_mouse_button_down(int x, int y, unsigned flags) 
{
   if(flags & 1)
   {
      double xd = x;
      double yd = y;
      double r = 4.0 / m_scale.scale();
      m_scale.inverse_transform(&xd, &yd);
      m_point_idx = m_shape.hit_test(xd, yd, r);
      force_redraw();
   }
   if(flags & 2)
   {
      m_hit_x = x;
      m_hit_y = y;
      force_redraw();
   }
}

void Flash_rasterizer::on_mouse_button_up(int x, int y, unsigned flags) 
{
   m_point_idx = -1;
   m_hit_x = -1;
   m_hit_y = -1;
   force_redraw();
}
