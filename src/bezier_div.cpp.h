#include "app_support.h"

class Bezier_div: public AppView
{
    agg::rgba8 m_ctrl_color;
    agg::bezier_ctrl<agg::rgba8> m_curve1;
    agg::slider_ctrl<agg::rgba8> m_angle_tolerance;
    agg::slider_ctrl<agg::rgba8> m_approximation_scale;
    agg::slider_ctrl<agg::rgba8> m_cusp_limit;
    agg::slider_ctrl<agg::rgba8> m_width;
    agg::cbox_ctrl<agg::rgba8>   m_show_points;
    agg::cbox_ctrl<agg::rgba8>   m_show_outline;
    agg::rbox_ctrl<agg::rgba8>   m_curve_type;
    agg::rbox_ctrl<agg::rgba8>   m_case_type;
    agg::rbox_ctrl<agg::rgba8>   m_inner_join;
    agg::rbox_ctrl<agg::rgba8>   m_line_join;
    agg::rbox_ctrl<agg::rgba8>   m_line_cap;

    int m_cur_case_type;

public:
    Bezier_div(App& app);
    template<class Curve> double measure_time(Curve& curve);
    template<class Path> 
    bool find_point(const Path& path, double dist, unsigned* i, unsigned* j);
    struct curve_point
    {
        curve_point() {}
        curve_point(double x1, double y1, double mu1) : x(x1), y(y1), mu(mu1) {}
        double x, y, dist, mu;
    };

    template<class Curve> double calc_max_error(Curve& curve, double scale, 
                                                double* max_angle_error);
    virtual void on_draw();
    virtual void on_key(int x, int y, unsigned key, unsigned flags);
    virtual void on_ctrl_change();
};
