#include "agg_color_rgba.h"
#include "agg_path_storage.h"
#include "agg_bounding_rect.h"

unsigned parse_lion(agg::path_storage& ps, agg::rgba8* colors, unsigned* path_idx);
