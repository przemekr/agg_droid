#include "app_support.h"
#include "ctrl/agg_spline_ctrl.h"

class Alpha_gradient : public AppView
{
    double m_x[3];
    double m_y[3];
    double m_dx;
    double m_dy;
    int    m_idx;
    agg::spline_ctrl<color_type> m_alpha;


public:
    Alpha_gradient(App& app);

    // A simple function to form the gradient color array 
    // consisting of 3 colors, "begin", "middle", "end"
    //---------------------------------------------------
    template<class ColorArrayT>
    static void fill_color_array(ColorArrayT& array, 
                                 color_type begin, 
                                 color_type middle, 
                                 color_type end);

    virtual void on_draw();

    virtual void on_mouse_button_down(int x, int y, unsigned flags);

    virtual void on_mouse_move(int x, int y, unsigned flags);

    virtual void on_mouse_button_up(int x, int y, unsigned flags);
    
    virtual void on_key(int x, int y, unsigned key, unsigned flags);
};
