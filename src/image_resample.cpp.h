#include "app_support.h"
#include "interactive_polygon.h"

class Image_resample: public AppView
{
   enum base_scale_e { base_shift = color_type::base_shift };
   typedef color_type::value_type                         value_type;
public:
    agg::gamma_lut<value_type, value_type, base_shift, base_shift> m_gamma_lut;
    agg::interactive_polygon     m_quad;
    agg::rbox_ctrl<agg::rgba>    m_trans_type;
    agg::slider_ctrl<agg::rgba>  m_gamma;
    agg::slider_ctrl<agg::rgba>  m_blur;
    double m_old_gamma;

    Image_resample(App& app);
    virtual void on_init();
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
    virtual void on_key(int x, int y, unsigned key, unsigned flags);
};
