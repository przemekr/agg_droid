#include "app_support.h"

class Conv_dash_marker: public AppView
{
    double m_x[3];
    double m_y[3];
    double m_dx;
    double m_dy;
    int    m_idx;
    agg::rbox_ctrl<agg::rgba8>   m_cap;
    agg::slider_ctrl<agg::rgba8> m_width;
    agg::slider_ctrl<agg::rgba8> m_smooth;
    agg::cbox_ctrl<agg::rgba8>   m_close;
    agg::cbox_ctrl<agg::rgba8>   m_even_odd;


public:
    Conv_dash_marker(App&);
    virtual void on_init();
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
    virtual void on_key(int x, int y, unsigned key, unsigned flags);
};
