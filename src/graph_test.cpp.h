#include "app_support.h"

//============================================================================
class graph
{
public:
   struct node
   {
      double x, y;
      node() {}
      node(double x_, double y_) : x(x_), y(y_) {}
   };

   struct edge
   {
      int node1;
      int node2;
      edge() {}
      edge(int n1, int n2) : node1(n1), node2(n2) {}
   };

   ~graph()
   {
      delete [] m_edges;
      delete [] m_nodes;
   }

   graph(int num_nodes, int num_edges) :
      m_num_nodes(num_nodes),
      m_num_edges(num_edges),
      m_nodes(new node[num_nodes]),
      m_edges(new edge[num_edges])
   {
      int i;

      srand(100);

      for(i = 0; i < m_num_nodes; i++)
      {
         m_nodes[i].x = (double(rand()) / RAND_MAX) * 0.75 + 0.2;
         m_nodes[i].y = (double(rand()) / RAND_MAX) * 0.85 + 0.1;
      }

      for(i = 0; i < m_num_edges; i++)
      {
         m_edges[i].node1 = rand() % m_num_nodes;
         m_edges[i].node2 = rand() % m_num_nodes;
         if(m_edges[i].node1 == m_edges[i].node2) i--;
      }
   }

   int get_num_nodes() const { return m_num_nodes; } 
   int get_num_edges() const { return m_num_edges; } 

   node get_node(int idx, double w, double h) const
   {
      node p(0.0, 0.0);
      if(idx < m_num_nodes)
      {
         p = m_nodes[idx];
         p.x = p.x * w;
         p.y = p.y * h;
      }
      return p;
   }

   edge get_edge(int idx) const
   {
      edge b(0,0);
      if(idx < m_num_edges)
      {
         b = m_edges[idx];
      }
      return b;
   }

private:
   graph(const graph&);
   const graph& operator = (const graph&);

   int m_num_nodes;
   int m_num_edges;
   node* m_nodes;
   edge* m_edges;
};

typedef agg::pod_auto_array<color_type, 256> color_array_type;
typedef agg::renderer_scanline_aa_solid<renderer_base_type>  solid_renderer;
typedef agg::renderer_scanline_bin_solid<renderer_base_type> draft_renderer;


class Graph_test: public AppView
{
    agg::rbox_ctrl<agg::rgba>   m_type;
    agg::slider_ctrl<agg::rgba> m_width;
    agg::cbox_ctrl<agg::rgba>   m_benchmark;
    agg::cbox_ctrl<agg::rgba>   m_draw_nodes;
    agg::cbox_ctrl<agg::rgba>   m_draw_edges;
    agg::cbox_ctrl<agg::rgba>   m_draft;
    agg::cbox_ctrl<agg::rgba>   m_translucent;
    graph                       m_graph;
    color_array_type            m_gradient_colors;
    int                         m_draw;
    agg::scanline_u8            m_sl;


public:
    Graph_test(App& app);
    void draw_nodes_draft();
    void draw_nodes_fine(rasterizer_scanline& ras);
    template<class Source>
    void render_edge_fine(rasterizer_scanline& ras, 
                          solid_renderer& ren_fine,
                          draft_renderer& ren_draft,
                          Source& src);
    void draw_lines_draft();
    void draw_curves_draft();
    void draw_dashes_draft();
    void draw_lines_fine(rasterizer_scanline& ras, 
                         solid_renderer& solid,
                         draft_renderer& draft);
    void draw_curves_fine(rasterizer_scanline& ras, 
                          solid_renderer& solid,
                          draft_renderer& draft);
    void draw_dashes_fine(rasterizer_scanline& ras, 
                          solid_renderer& solid,
                          draft_renderer& draft);
    void draw_polygons(rasterizer_scanline& ras, 
                       solid_renderer& solid,
                       draft_renderer& draft);
    void draw_scene(rasterizer_scanline& ras, 
                    solid_renderer& solid,
                    draft_renderer& draft);
    virtual void on_draw();
    virtual void on_ctrl_change();
};
