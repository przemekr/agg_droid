#include "app_support.h"
#include "interactive_polygon.h"

class Pattern_perspective: public AppView
{
public:
    typedef agg::renderer_base<pixfmt_type>                renderer_base;
    typedef agg::renderer_scanline_aa_solid<renderer_base> renderer_solid;
    typedef agg::renderer_base<pixfmt_pre> renderer_base_pre;

    agg::interactive_polygon  m_quad;
    agg::rbox_ctrl<agg::rgba> m_trans_type;
    bool m_test_flag;

    Pattern_perspective(App& app);
    virtual void on_init();
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
};
