#include "app_support.h"

class Gpc_test: public AppView
{
    agg::rbox_ctrl<agg::rgba8> m_polygons;
    agg::rbox_ctrl<agg::rgba8> m_operation;
    double m_x;
    double m_y;

public:
    Gpc_test(App& app);
    template<class Scanline, class Ras, class Ren, class Gpc>
    void perform_rendering(Scanline& sl, Ras& ras, Ren& ren, Gpc& gpc);
    template<class Scanline, class Ras>
    unsigned render_gpc(Scanline& sl, Ras& ras);
    virtual void on_init();
    virtual void on_draw();
    //double random(double min, double max);
    //virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
};
