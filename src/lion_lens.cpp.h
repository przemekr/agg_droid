#include "app_support.h"

class Lion_lens: public AppView
{
    agg::slider_ctrl<agg::rgba8> m_magn_slider;
    agg::slider_ctrl<agg::rgba8> m_radius_slider;

public:
    typedef agg::renderer_base<pixfmt_type> renderer_base;
    typedef agg::renderer_scanline_aa_solid<renderer_base> renderer_solid;

    Lion_lens(App& app);
    virtual void on_init();
    virtual void on_resize(int cx, int cy);
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
};
