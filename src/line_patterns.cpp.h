#include "app_support.h"

class Line_patterns: public AppView
{
    agg::rgba8 m_ctrl_color;
    agg::bezier_ctrl<agg::rgba8> m_curve1;
    agg::bezier_ctrl<agg::rgba8> m_curve2;
    agg::bezier_ctrl<agg::rgba8> m_curve3;
    agg::bezier_ctrl<agg::rgba8> m_curve4;
    agg::bezier_ctrl<agg::rgba8> m_curve5;
    agg::bezier_ctrl<agg::rgba8> m_curve6;
    agg::bezier_ctrl<agg::rgba8> m_curve7;
    agg::bezier_ctrl<agg::rgba8> m_curve8;
    agg::bezier_ctrl<agg::rgba8> m_curve9;
    agg::slider_ctrl<agg::rgba8> m_scale_x;
    agg::slider_ctrl<agg::rgba8> m_start_x;

public:
    typedef agg::renderer_base<pixfmt_type> renderer_base;
    typedef agg::renderer_scanline_aa_solid<renderer_base> renderer_scanline;
    typedef agg::rasterizer_scanline_aa<> rasterizer_scanline;
    typedef agg::scanline_p8 scanline;


    Line_patterns(App& app);
    template<class Pattern, 
             class Rasterizer, 
             class Renderer, 
             class PatternSource, 
             class VertexSource>
    void draw_curve(Pattern& patt, 
                    Rasterizer& ras, 
                    Renderer& ren, 
                    PatternSource& src, 
                    VertexSource& vs);

    virtual void on_draw();
    virtual void on_key(int x, int y, unsigned key, unsigned flags);
    virtual void on_ctrl_change();
};
