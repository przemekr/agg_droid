#include "app_support.h"

class Idea: public AppView
{
    double m_dx;
    double m_dy;
    agg::cbox_ctrl<agg::rgba8> m_rotate;
    agg::cbox_ctrl<agg::rgba8> m_even_odd;
    agg::cbox_ctrl<agg::rgba8> m_draft;
    agg::cbox_ctrl<agg::rgba8> m_roundoff;
    agg::slider_ctrl<agg::rgba8> m_angle_delta;
    bool m_redraw_flag;

public:
    Idea(App& app);
    virtual void on_init();
    virtual void on_resize(int, int);
    virtual void on_draw();
    virtual void on_idle();
    virtual void on_ctrl_change();
};
