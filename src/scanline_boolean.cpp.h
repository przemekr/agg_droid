#include "app_support.h"
#include "interactive_polygon.h"

class Scanline_boolean: public AppView
{
public:
    typedef agg::renderer_base<pixfmt_type> renderer_base;
    typedef agg::renderer_scanline_aa_solid<renderer_base> renderer_solid;
    typedef agg::scanline_p8 scanline_type;

    agg::interactive_polygon     m_quad1;
    agg::interactive_polygon     m_quad2;
    agg::rbox_ctrl<agg::rgba8>   m_trans_type;
    agg::cbox_ctrl<agg::rgba8>   m_reset;
    agg::slider_ctrl<agg::rgba8> m_mul1;
    agg::slider_ctrl<agg::rgba8> m_mul2;

    Scanline_boolean(App& app);
    virtual void on_init();
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
    virtual void on_ctrl_change();
};
