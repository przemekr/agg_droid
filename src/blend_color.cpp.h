#include "app_support.h"

class Blend_color: public AppView
{
    agg::rbox_ctrl<agg::rgba8>    m_method;
    agg::slider_ctrl<agg::rgba8>  m_radius;
    agg::polygon_ctrl<agg::rgba8> m_shadow_ctrl;

    agg::path_storage             m_path;
    typedef agg::conv_curve<agg::path_storage> shape_type;
    shape_type                    m_shape;

    agg::rasterizer_scanline_aa<> m_ras;
    agg::scanline_p8              m_sl;

    agg::rect_d m_shape_bounds;

    agg::pod_array<agg::int8u> m_gray8_buf;
    agg::rendering_buffer      m_gray8_rbuf;
    agg::rendering_buffer      m_gray8_rbuf2;

    agg::pod_array<color_type> m_color_lut;
public:
    Blend_color(App& app);
    virtual void on_resize(int sx, int sy);
    virtual void on_draw();
};
