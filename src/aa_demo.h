#include "app_support.h"
#include "agg_basics.h"
#include "agg_rendering_buffer.h"
#include "agg_rasterizer_scanline_aa.h"
#include "agg_scanline_u.h"
#include "agg_renderer_scanline.h"
#include "agg_pixfmt_rgb.h"
#include "platform/agg_platform_support.h"
#include "ctrl/agg_slider_ctrl.h"
#include "ctrl/agg_cbox_ctrl.h"

class Aa_demo : public View
{
public:
    Aa_demo(App& application);

    virtual ~Aa_demo() { }

    virtual void on_init() { }

    virtual void on_draw();

    virtual void on_mouse_button_down(int x, int y, unsigned flags);

    virtual void on_mouse_move(int x, int y, unsigned flags);

    virtual void on_mouse_button_up(int x, int y, unsigned flags);

private:
    App& app;
    double m_x[3];
    double m_y[3];
    double m_dx;
    double m_dy;
    int    m_idx;

    agg::slider_ctrl<agg::rgba8> m_slider1;
    agg::slider_ctrl<agg::rgba8> m_slider2;
};
