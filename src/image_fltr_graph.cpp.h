#include "app_support.h"
#include "agg_span_image_filter_rgb.h"

struct filter_base
{
   virtual double radius() const = 0;
   virtual void set_radius(double r) = 0;
   virtual double calc_weight(double x) const = 0;
};


template<class Filter> struct image_filter_const_radius_adaptor : filter_base
{
   double radius() const { return m_filter.radius(); }
   void set_radius(double r) {}
   double calc_weight(double x) const { return m_filter.calc_weight(fabs(x)); }
   Filter m_filter;
};


template<class Filter> struct image_filter_variable_radius_adaptor : filter_base
{
   double radius() const { return m_filter.radius(); }
   double calc_weight(double x) const { return m_filter.calc_weight(fabs(x)); }
   void set_radius(double r) { m_filter = Filter(r); }
   image_filter_variable_radius_adaptor() : m_filter(2.0) {}
   Filter m_filter;
};

class Image_fltr_graph: public AppView
{
    agg::slider_ctrl<agg::rgba>  m_radius;
    agg::cbox_ctrl<agg::rgba>  m_bilinear;
    agg::cbox_ctrl<agg::rgba>  m_bicubic;
    agg::cbox_ctrl<agg::rgba>  m_spline16;
    agg::cbox_ctrl<agg::rgba>  m_spline36;
    agg::cbox_ctrl<agg::rgba>  m_hanning;
    agg::cbox_ctrl<agg::rgba>  m_hamming;
    agg::cbox_ctrl<agg::rgba>  m_hermite;
    agg::cbox_ctrl<agg::rgba>  m_kaiser;
    agg::cbox_ctrl<agg::rgba>  m_quadric;
    agg::cbox_ctrl<agg::rgba>  m_catrom;
    agg::cbox_ctrl<agg::rgba>  m_gaussian;
    agg::cbox_ctrl<agg::rgba>  m_bessel;
    agg::cbox_ctrl<agg::rgba>  m_mitchell;
    agg::cbox_ctrl<agg::rgba>  m_sinc;
    agg::cbox_ctrl<agg::rgba>  m_lanczos;
    agg::cbox_ctrl<agg::rgba>  m_blackman;
    agg::cbox_ctrl<agg::rgba>* m_filters[32];

    image_filter_const_radius_adaptor<agg::image_filter_bilinear>    m_filter_bilinear;
    image_filter_const_radius_adaptor<agg::image_filter_bicubic>     m_filter_bicubic;
    image_filter_const_radius_adaptor<agg::image_filter_spline16>    m_filter_spline16;
    image_filter_const_radius_adaptor<agg::image_filter_spline36>    m_filter_spline36;
    image_filter_const_radius_adaptor<agg::image_filter_hanning>     m_filter_hanning;
    image_filter_const_radius_adaptor<agg::image_filter_hamming>     m_filter_hamming;
    image_filter_const_radius_adaptor<agg::image_filter_hermite>     m_filter_hermite;
    image_filter_const_radius_adaptor<agg::image_filter_kaiser>      m_filter_kaiser;
    image_filter_const_radius_adaptor<agg::image_filter_quadric>     m_filter_quadric;
    image_filter_const_radius_adaptor<agg::image_filter_catrom>      m_filter_catrom;
    image_filter_const_radius_adaptor<agg::image_filter_gaussian>    m_filter_gaussian;
    image_filter_const_radius_adaptor<agg::image_filter_bessel>      m_filter_bessel;
    image_filter_const_radius_adaptor<agg::image_filter_mitchell>    m_filter_mitchell;
    image_filter_variable_radius_adaptor<agg::image_filter_sinc>     m_filter_sinc;
    image_filter_variable_radius_adaptor<agg::image_filter_lanczos>  m_filter_lanczos;
    image_filter_variable_radius_adaptor<agg::image_filter_blackman> m_filter_blackman;

    filter_base* m_filter_func[32];
    unsigned     m_num_filters;


public:
    Image_fltr_graph(App& app);
    virtual ~Image_fltr_graph();
    virtual void on_draw();
};
