#include "app_support.h"

class Component_rendering: public AppView
{
    agg::slider_ctrl<agg::rgba8> m_alpha;

public:
    Component_rendering(App&);
    virtual void on_draw();

};
