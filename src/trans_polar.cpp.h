#include "app_support.h"

class Trans_polar: public AppView
{
    agg::slider_ctrl<agg::rgba8> m_slider1;
    agg::slider_ctrl<agg::rgba8> m_slider_spiral;
    agg::slider_ctrl<agg::rgba8> m_slider_base_y;

public:
    Trans_polar(App& app);
    virtual ~Trans_polar();
    virtual void on_init();
    virtual void on_draw();
};
