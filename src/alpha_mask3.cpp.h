#include "app_support.h"

class Alpha_mask3: public AppView
{
    agg::rbox_ctrl<agg::rgba8> m_polygons;
    agg::rbox_ctrl<agg::rgba8> m_operation;

    typedef agg::amask_no_clip_gray8 alpha_mask_type;
    //typedef agg::alpha_mask_gray8 alpha_mask_type;

    typedef agg::pixfmt_bgr24 pixfmt_type;

    unsigned char* m_alpha_buf;
    agg::rendering_buffer m_alpha_mask_rbuf;
    alpha_mask_type m_alpha_mask;
    agg::rasterizer_scanline_aa<> m_ras;
    agg::scanline_p8              m_sl;

    double m_x;
    double m_y;

public:
    ~Alpha_mask3();
    Alpha_mask3(App& app);
    void draw_text(double x, double y, const char* str);
    template<class VertexSource> void generate_alpha_mask(VertexSource& vs);
    template<class VertexSource>
    void perform_rendering(VertexSource& vs);
    unsigned render();
    virtual void on_init();
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
};
