#include "app_support.h"

class Rounded_rect: public AppView
{
    double m_x[2];
    double m_y[2];
    double m_dx;
    double m_dy;
    int    m_idx;
    agg::slider_ctrl<agg::rgba8> m_radius;
    agg::slider_ctrl<agg::rgba8> m_gamma;
    agg::slider_ctrl<agg::rgba8> m_offset;
    agg::cbox_ctrl<agg::rgba8>   m_white_on_black;


public:
    Rounded_rect(App& app);
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
};
