#include "app_support.h"

class Compositing: public AppView
{
   typedef color_type::value_type value_type;
   typedef agg::order_bgra order;
   typedef agg::rendering_buffer rbuf_type;
   typedef agg::int32u pixel_type;
   typedef agg::blender_rgba<color_type, order> prim_blender_type; 
   typedef agg::pixfmt_alpha_blend_rgba<prim_blender_type, rbuf_type, pixel_type> prim_pixfmt_type;
   typedef agg::renderer_base<prim_pixfmt_type> prim_ren_base_type;

   agg::slider_ctrl<color_type> m_alpha_src;
   agg::slider_ctrl<color_type> m_alpha_dst;
   agg::rbox_ctrl<color_type>   m_comp_op;

public:
    Compositing(App&);
    virtual ~Compositing();
    virtual void on_init();
    void render_scene(rbuf_type& rbuf, prim_pixfmt_type& pixf);
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
};
