#include "app_support.h"

class Image_transforms: public AppView
{
    agg::slider_ctrl<agg::rgba> m_polygon_angle;
    agg::slider_ctrl<agg::rgba> m_polygon_scale;

    agg::slider_ctrl<agg::rgba> m_image_angle;
    agg::slider_ctrl<agg::rgba> m_image_scale;

    agg::cbox_ctrl<agg::rgba> m_rotate_polygon;
    agg::cbox_ctrl<agg::rgba> m_rotate_image;

    agg::rbox_ctrl<agg::rgba> m_example;

    double m_image_center_x;
    double m_image_center_y;

    double m_polygon_cx;
    double m_polygon_cy;

    double m_image_cx;
    double m_image_cy;

    double m_dx;
    double m_dy;

    int m_flag;


public:
    Image_transforms(App& app);
    virtual ~Image_transforms();
    virtual void on_init();
    void create_star(agg::path_storage& ps);
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
    virtual void on_ctrl_change();
    virtual void on_idle() ;
};
