#include "app_support.h"

class Lion: public AppView
{
    agg::slider_ctrl<agg::rgba8> m_alpha_slider;

public:
    typedef agg::renderer_base<pixfmt_type> renderer_base;
    typedef agg::renderer_scanline_aa_solid<renderer_base> renderer_solid;

    Lion(App& app);
    virtual void on_resize(int cx, int cy);
    virtual void on_draw();
    void transform(double width, double height, double x, double y);
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
};
