#include "app_support.h"

class Alpha_mask : public AppView
{
    unsigned char* m_alpha_buf;
    agg::rendering_buffer m_alpha_rbuf;

public:
    virtual ~Alpha_mask();
    Alpha_mask(App& app);
    void generate_alpha_mask(int cx, int cy);
    virtual void on_resize(int cx, int cy);
    virtual void on_draw();
    void transform(double width, double height, double x, double y);
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
};
