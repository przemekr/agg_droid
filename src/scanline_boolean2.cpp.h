#include "app_support.h"

class Scanline_boolean2: public AppView
{
    typedef agg::pixfmt_bgr24 pixfmt_type;

    agg::rbox_ctrl<agg::rgba8> m_polygons;
    agg::rbox_ctrl<agg::rgba8> m_fill_rule;
    agg::rbox_ctrl<agg::rgba8> m_scanline_type;
    agg::rbox_ctrl<agg::rgba8> m_operation;
    double m_x;
    double m_y;

public:
    Scanline_boolean2(App& app);
    template<class Rasterizer>
    void render_scanline_boolean(Rasterizer& ras1, Rasterizer& ras2);
    template<class Rasterizer>
    unsigned render_sbool(Rasterizer& ras1, Rasterizer& ras2);
    virtual void on_init();
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
};
