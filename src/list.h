#include "ctrl/agg_slider_ctrl.h"
#include "ctrl/agg_rbox_ctrl.h"
#include "ctrl/agg_cbox_ctrl.h"
#include "agg_button_ctrl.h"
#include "agg_span_image_filter_rgb.h"

#include <vector>
#include <string>

class ListView : public AppView
{
public:
   ListView(App& app): AppView(app)
   {
   }

   void add(std::string s)
   {
      static int y = 10;
      static int x = 10;
      if (y + 30 > START_H - 10)
      {
         y = 10;
         x += 120;
      }
      char label[13];
      snprintf(label, sizeof(label), "%.12s", s.c_str());
      agg::button_ctrl<color_type>* b = new agg::button_ctrl<color_type>(x, y, x+100, y+30, label);
      agg::rgba c(0.9,0,0,0.9);
      b->background_color(c);
      y +=30;
      entries.push_back(ElemType(s, b));
      add_ctrl(*b);
   }

   void enter()
   {
      app.wait_mode(true);
      force_redraw();
   }

   virtual void on_draw()
   {
      pixfmt_type  pf(app.rbuf_window());
      renderer_base_type rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      rbase.clear(lgray);

      /* Scale background image */
      int w = width();
      int h = height();
      double scaleX = (double)w/app.rbuf_img(IMG_AGG).width();
      double scaleY = (double)h/app.rbuf_img(IMG_AGG).height();
      static agg::trans_affine shape_mtx; shape_mtx.reset();
      shape_mtx *= agg::trans_affine_scaling(scaleX, scaleY);
      shape_mtx *= agg::trans_affine_translation(0, 0);
      shape_mtx.invert();

      typedef agg::span_interpolator_linear<agg::trans_affine> interpolator_type;
      interpolator_type interpolator(shape_mtx);
      typedef agg::image_accessor_clone<pixfmt_type> img_accessor_type;
      pixfmt_type pixf_img(app.rbuf_img(IMG_AGG));
      img_accessor_type ia(pixf_img);
      typedef agg::span_image_filter_rgb_nn<img_accessor_type,
              interpolator_type> span_gen_type;
      span_gen_type sg(ia, interpolator);
      agg::span_allocator<color_type> sa;
      ras.move_to_d(0,0);
      ras.line_to_d(w,0);
      ras.line_to_d(w,h);
      ras.line_to_d(0,h);
      agg::render_scanlines_aa(ras, sl, rbase, sa, sg);
      for (Entries::iterator i = entries.begin(); i != entries.end(); i++)
      {
         agg::render_ctrl(ras, sl, rbase, *i->second);
      }

   }

   virtual void on_ctrl_change()
   {
      for (Entries::iterator i = entries.begin(); i != entries.end(); i++)
      {
         if (i->second->status())
         {
            app.changeView(i->first.c_str());
            i->second->status(false);
         }
      }
   }

private:
   typedef std::pair <std::string, agg::button_ctrl<color_type>* > ElemType;
   typedef std::vector<ElemType> Entries;
   Entries entries;
};
