#ifndef GOURAUD_MESH__
#define GOURAUD_MESH__

#include "app_support.h"
#include "agg_span_gouraud_rgba.h"

namespace agg
{

   struct mesh_point
   {
      double x,y;
      double dx,dy;
      rgba8 color;
      rgba8 dc;

      mesh_point() {}
      mesh_point(double x_, double y_, 
            double dx_, double dy_, 
            rgba8 c, rgba8 dc_) : 
         x(x_), y(y_), 
         dx(dx_), dy(dy_), 
         color(c), dc(dc_)
      {}
   };

   struct mesh_triangle
   {
      unsigned p1, p2, p3;

      mesh_triangle() {}
      mesh_triangle(unsigned i, unsigned j, unsigned k) : 
         p1(i), p2(j), p3(k) 
      {}
   };

   struct mesh_edge
   {
      unsigned p1, p2;
      int      tl, tr;

      mesh_edge() {}
      mesh_edge(unsigned p1_, unsigned p2_, int tl_, int tr_) :
         p1(p1_), p2(p2_), tl(tl_), tr(tr_) 
      {}
   };


   static double random(double v1, double v2)
   {
      return (v2 - v1) * (rand() % 1000) / 999.0 + v1;
   }


   class mesh_ctrl
   {
   public:
      mesh_ctrl();

      void generate(unsigned cols, unsigned rows, 
            double cell_w, double cell_h,
            double start_x, double start_y);

      void randomize_points(double delta); 
      void rotate_colors();


      bool on_mouse_button_down(double x, double y, unsigned flags);
      bool on_mouse_move(double x, double y, unsigned flags);
      bool on_mouse_button_up(double x, double y, unsigned flags);

      unsigned num_vertices() const { return m_vertices.size(); }
      const mesh_point& vertex(unsigned i) const { return m_vertices[i]; }
      mesh_point& vertex(unsigned i)       { return m_vertices[i]; }

      const mesh_point& vertex(unsigned x, unsigned y) const { return m_vertices[y * m_rows + x]; }
      mesh_point& vertex(unsigned x, unsigned y)       { return m_vertices[y * m_rows + x]; }

      unsigned num_triangles() const { return m_triangles.size(); }
      const mesh_triangle& triangle(unsigned i) const { return m_triangles[i]; }
      mesh_triangle& triangle(unsigned i)       { return m_triangles[i]; }

      unsigned num_edges() const { return m_edges.size(); }
      const mesh_edge& edge(unsigned i) const { return m_edges[i]; }
      mesh_edge& edge(unsigned i)       { return m_edges[i]; }

   private:
      unsigned m_cols;
      unsigned m_rows;
      int      m_drag_idx;
      double   m_drag_dx;
      double   m_drag_dy;
      double   m_cell_w;
      double   m_cell_h;
      double   m_start_x;
      double   m_start_y;
      pod_bvector<mesh_point>    m_vertices;
      pod_bvector<mesh_triangle> m_triangles;
      pod_bvector<mesh_edge>     m_edges;
   };

   class styles_gouraud
   {
   public:
      typedef span_gouraud_rgba<rgba8> gouraud_type;

      template<class Gamma>
         styles_gouraud(const mesh_ctrl& mesh, const Gamma& gamma)
         {
            unsigned i;
            for(i = 0; i < mesh.num_triangles(); i++)
            {
               const agg::mesh_triangle& t = mesh.triangle(i);
               const agg::mesh_point& p1 = mesh.vertex(t.p1);
               const agg::mesh_point& p2 = mesh.vertex(t.p2);
               const agg::mesh_point& p3 = mesh.vertex(t.p3);

               agg::rgba8 c1 = p1.color; 
               agg::rgba8 c2 = p2.color; 
               agg::rgba8 c3 = p3.color;
               c1.apply_gamma_dir(gamma);
               c2.apply_gamma_dir(gamma);
               c3.apply_gamma_dir(gamma);
               gouraud_type gouraud(c1, c2, c3,
                     p1.x, p1.y, 
                     p2.x, p2.y,
                     p3.x, p3.y);
               gouraud.prepare();
               m_triangles.add(gouraud);
            }
         }

      bool is_solid(unsigned style) const { return false; }

      rgba8 color(unsigned style) const { return rgba8(0,0,0,0); }

      void generate_span(rgba8* span, int x, int y, unsigned len, unsigned style)
      {
         m_triangles[style].generate(span, x, y, len);
      }

   private:
      pod_bvector<gouraud_type> m_triangles;
   };
}

class Gouraud_mesh: public AppView
{
public:
    typedef agg::renderer_base<pixfmt_type> renderer_base;
    typedef agg::renderer_scanline_aa_solid<renderer_base> renderer_scanline;
    typedef agg::rasterizer_scanline_aa<> rasterizer_scanline;
    typedef agg::scanline_u8 scanline;

    agg::mesh_ctrl      m_mesh;
    agg::gamma_lut<>    m_gamma;


    Gouraud_mesh(App& app);
    virtual void on_init();
    virtual void on_draw();
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
    virtual void on_key(int x, int y, unsigned key, unsigned flags);
    void on_idle();
    virtual void on_ctrl_change();
};



#endif /* end of: GOURAUD_MESH.CPP__ */
