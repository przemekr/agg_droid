#include "app_support.h"

class Conv_contour: public AppView
{
    agg::rbox_ctrl<agg::rgba8>   m_close;
    agg::slider_ctrl<agg::rgba8> m_width;
    agg::cbox_ctrl<agg::rgba8>   m_auto_detect;
    agg::path_storage            m_path;

public:
    Conv_contour(App&);
    void compose_path();
    virtual void on_draw();
};
