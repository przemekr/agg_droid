#include "app_support.h"

class Gouraud: public AppView
{
    double m_x[3];
    double m_y[3];
    double m_dx;
    double m_dy;
    int    m_idx;

    agg::slider_ctrl<agg::rgba> m_dilation;
    agg::slider_ctrl<agg::rgba> m_gamma;
    agg::slider_ctrl<agg::rgba> m_alpha;

public:
    Gouraud(App& app);
    template<class Scanline, class Ras> 
    void render_gouraud(Scanline& sl, Ras& ras);
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
    virtual void on_key(int x, int y, unsigned key, unsigned flags);
};
