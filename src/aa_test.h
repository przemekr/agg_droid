#include <stdio.h>
#include "agg_basics.h"
#include "agg_rendering_buffer.h"
#include "agg_rasterizer_scanline_aa.h"
#include "agg_scanline_u.h"
#include "agg_renderer_scanline.h"
#include "agg_pixfmt_rgb.h"
#include "agg_gamma_lut.h"
#include "agg_conv_dash.h"
#include "agg_conv_stroke.h"
#include "agg_span_gradient.h"
#include "agg_span_interpolator_linear.h"
#include "agg_span_gouraud_rgba.h"
#include "agg_span_allocator.h"
#include "platform/agg_platform_support.h"
#include "ctrl/agg_slider_ctrl.h"
#include "ctrl/agg_cbox_ctrl.h"
#include "app_support.h"

typedef agg::gamma_lut<agg::int8u, agg::int8u, 8, 8>        gamma_lut_type;
typedef agg::scanline_u8                                    scanline_type;
typedef agg::pixfmt_bgr24_gamma<gamma_lut_type>             pixfmt_gamma_type;

class Aa_test : public AppView
{
    gamma_lut_type              m_gamma;
    agg::slider_ctrl<agg::rgba> m_slider_gamma;

public:
    Aa_test(App& application);

    virtual void on_draw();

    virtual void on_mouse_button_down(int x, int y, unsigned flags);
};

