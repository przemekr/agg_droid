/*
 * 24-game, a simple arithmetic game.
 * Copyright 2014 Przemyslaw Rzepecki
 * Contact: przemekr@sdfeu.org
 * 
 * This file is part of 24-game.
 * 
 * 24-game is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * 24-game is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * 24-game.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <vector>
#include <string>

#include <math.h>
#include <algorithm> 
#include <stack>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include "app_support.h"
#include "aa_demo.h"
#include "aa_test.h"
#include "alpha_gradient.h"
#include "alpha_mask.cpp.h"
#include "alpha_mask2.cpp.h"
#include "alpha_mask3.cpp.h"
#include "bezier_div.cpp.h"
#include "blend_color.cpp.h"
#include "blur.cpp.h"
#include "bspline.cpp.h"
#include "circles.cpp.h"
#include "component_rendering.cpp.h"
#include "compositing.cpp.h"
#include "compositing2.cpp.h"
#include "conv_contour.cpp.h"
#include "conv_dash_marker.cpp.h"
#include "conv_stroke.cpp.h"
#include "distortions.cpp.h"
#include "flash_rasterizer.cpp.h"
#include "gamma_correction.cpp.h"
#include "gamma_ctrl.cpp.h"
#include "gamma_tuner.cpp.h"
#include "gouraud.cpp.h"
#include "gouraud_mesh.cpp.h"
#include "gpc_test.cpp.h"
#include "gradient_focal.cpp.h"
#include "gradients.cpp.h"
#include "graph_test.cpp.h"
#include "idea.cpp.h"
#include "image1.cpp.h"
#include "image_alpha.cpp.h"
#include "image_filters.cpp.h"
#include "image_filters2.cpp.h"
#include "image_fltr_graph.cpp.h"

#include "image_perspective.cpp.h"
#include "image_resample.cpp.h"
#include "image_transforms.cpp.h"
#include "line_patterns_clip.cpp.h"
#include "line_patterns.cpp.h"
#include "lion.cpp.h"
#include "lion_lens.cpp.h"
#include "lion_outline.cpp.h"
#include "mol_view.cpp.h"
#include "multi_clip.cpp.h"
#include "pattern_fill.cpp.h"
#include "pattern_perspective.cpp.h"
#include "pattern_resample.cpp.h"
#include "rasterizer_compound.cpp.h"
#include "raster_text.cpp.h"
#include "rounded_rect.cpp.h"
#include "scanline_boolean2.cpp.h"
#include "scanline_boolean.cpp.h"
#include "simple_blur.cpp.h"
#include "trans_curve1_ft.cpp.h"
#include "trans_curve2_ft.cpp.h"
#include "trans_polar.cpp.h"

#include "list.h"


#define ADDVIEW(x) add(#x, new x(*this))

class the_application: public App
{
public:
   the_application(agg::pix_format_e format, bool flip_y) :
      App(format, flip_y)
   {
      srand(time(NULL)); 
      list = new ListView(*this);

      ADDVIEW(Aa_demo);
      ADDVIEW(Aa_test);
      ADDVIEW(Alpha_gradient);
      ADDVIEW(Alpha_mask);
      ADDVIEW(Alpha_mask2);
      ADDVIEW(Alpha_mask3);
      ADDVIEW(Bezier_div);
      ADDVIEW(Blend_color);
      ADDVIEW(Blur);
      ADDVIEW(Bspline);
      ADDVIEW(Circles);
      ADDVIEW(Component_rendering);
      //ADDVIEW(Compositing);
      //ADDVIEW(Compositing2);
      ADDVIEW(Conv_contour);
      ADDVIEW(Conv_dash_marker);
      ADDVIEW(Conv_stroke);
      ADDVIEW(Distortions);
      ADDVIEW(Flash_rasterizer);
      ADDVIEW(Gamma_correction);
      ADDVIEW(Gamma_ctrl);
      ADDVIEW(Gamma_tuner);
      ADDVIEW(Gouraud);
      ADDVIEW(Gouraud_mesh);
      ADDVIEW(Gpc_test);
      ADDVIEW(Gradient_focal);
      ADDVIEW(Gradients);
      ADDVIEW(Graph_test);
      ADDVIEW(Idea);
      ADDVIEW(Image1);
      ADDVIEW(Image_alpha);
      ADDVIEW(Image_filters);
      ADDVIEW(Image_filters2);
      ADDVIEW(Image_fltr_graph);
      //ADDVIEW(Image_perspective);
      ADDVIEW(Image_resample);
      //ADDVIEW(Image_transforms);
      ADDVIEW(Line_patterns_clip);
      ADDVIEW(Line_patterns);
      ADDVIEW(Lion);
      ADDVIEW(Lion_lens);
      ADDVIEW(Lion_outline);
      ADDVIEW(Mol_view);
      ADDVIEW(Multi_clip);
      ADDVIEW(Pattern_fill);
      ADDVIEW(Pattern_perspective);
      ADDVIEW(Pattern_resample);
      //ADDVIEW(Rasterizer_compound);
      ADDVIEW(Raster_text);
      ADDVIEW(Rounded_rect);
      ADDVIEW(Scanline_boolean2);
      ADDVIEW(Scanline_boolean);
      ADDVIEW(Simple_blur);
      ADDVIEW(Trans_curve1_ft);
      ADDVIEW(Trans_curve2_ft);
      ADDVIEW(Trans_polar);

      changeView("list");
   }
   virtual void changeView(const char* name) 
   {
      printf("change view %s\n", name);

      for (Views::iterator i = views.begin(); i != views.end(); i++)
      {
         if (strcmp(i->first.c_str(), name) == 0)
         {
            char buf[128];
            sprintf(buf, "AGG-Examples: %s", name);
            caption(buf);

            view = i->second;
         }
      }
      if (strcmp(name, "list") == 0)
         view = list;

      m_ctrls = view->m_ctrls;
      view->on_init();
      view->enter();
      view->on_resize(rbuf_window().width(), rbuf_window().height());
   };
private:
   typedef std::pair <std::string, View* > ElemType;
   typedef std::vector<ElemType> Views;
   Views views;
   ListView* list;

   void add(const char* s, View* v)
   {
      views.push_back(ElemType(s, v));
      list->add(s);
   }
};


int agg_main(int argc, char* argv[])
{
    //the_application app(agg::pix_format_bgra32, flip_y);
    the_application app(agg::pix_format_bgr24, flip_y);
    app.caption("AGG-Droid");

    if (false
          || !app.load_img(IMG_AGG,            "antigrain.png")
          || !app.load_img(IMG_COMPOSITING,    "compositing.png")
          || !app.load_img(IMG_SPHERES,        "spheres.png")
          || !app.load_img(IMG_LINE_PATTERN_0, "1.png")
          || !app.load_img(IMG_LINE_PATTERN_1, "2.png")
          || !app.load_img(IMG_LINE_PATTERN_2, "3.png")
          || !app.load_img(IMG_LINE_PATTERN_3, "4.png")
          || !app.load_img(IMG_LINE_PATTERN_4, "5.png")
          || !app.load_img(IMG_LINE_PATTERN_5, "6.png")
          || !app.load_img(IMG_LINE_PATTERN_6, "7.png")
          || !app.load_img(IMG_LINE_PATTERN_7, "8.png")
          || !app.load_img(IMG_LINE_PATTERN_8, "9.png")
          )
    {
        char buf[256];
        sprintf(buf, "Image file missing\n");
        app.message(buf);
        return 1;
    }
    if (app.init(START_W, START_H, WINDOW_FLAGS))
    {
       try {
          return app.run();
       } catch (...) {
          return 0;
       }
    }
    return 1;
}
