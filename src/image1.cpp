#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "image1.cpp.h"
#include "agg_rendering_buffer.h"
#include "agg_rasterizer_scanline_aa.h"
#include "agg_ellipse.h"
#include "agg_trans_affine.h"
#include "agg_conv_transform.h"
#include "agg_span_image_filter_rgb.h"
#include "agg_span_image_filter_rgba.h"
#include "agg_span_image_filter_gray.h"
#include "agg_pixfmt_rgba.h"
#include "agg_scanline_u.h"
#include "agg_renderer_scanline.h"
#include "agg_span_allocator.h"
#include "agg_span_interpolator_linear.h"
#include "agg_image_accessors.h"
#include "ctrl/agg_slider_ctrl.h"
#include "platform/agg_platform_support.h"

Image1::Image1(App& app):
   AppView(app),
   m_angle(5,  5,    300, 12,    !flip_y),
   m_scale(5,  5+15, 300, 12+15, !flip_y)
{
   add_ctrl(m_angle);
   add_ctrl(m_scale);
   m_angle.label("Angle=%3.2f");
   m_scale.label("Scale=%3.2f");
   m_angle.range(-180.0, 180.0);
   m_angle.value(0.0);
   m_scale.range(0.1, 5.0);
   m_scale.value(1.0);
}

Image1::~Image1()
{
}

void Image1::on_draw()
{
   typedef agg::renderer_base<pixfmt_type>     renderer_base;
   typedef agg::renderer_base<pixfmt_pre> renderer_base_pre;

   pixfmt_type       pixf(rbuf_window());
   pixfmt_pre        pixf_pre(rbuf_window());
   renderer_base     rb(pixf);
   renderer_base_pre rb_pre(pixf_pre);

   rb.clear(agg::rgba(1.0, 1.0, 1.0));

   agg::trans_affine src_mtx;
   src_mtx *= agg::trans_affine_translation(-app.initial_width()/2 - 10, -app.initial_height()/2 - 20 - 10);
   src_mtx *= agg::trans_affine_rotation(m_angle.value() * agg::pi / 180.0);
   src_mtx *= agg::trans_affine_scaling(m_scale.value());
   src_mtx *= agg::trans_affine_translation(app.initial_width()/2, app.initial_height()/2 + 20);
   src_mtx *= app.trans_affine_resizing();

   agg::trans_affine img_mtx;
   img_mtx *= agg::trans_affine_translation(-app.initial_width()/2 + 10, -app.initial_height()/2 + 20 + 10);
   img_mtx *= agg::trans_affine_rotation(m_angle.value() * agg::pi / 180.0);
   img_mtx *= agg::trans_affine_scaling(m_scale.value());
   img_mtx *= agg::trans_affine_translation(app.initial_width()/2, app.initial_height()/2 + 20);
   img_mtx *= app.trans_affine_resizing();
   img_mtx.invert();

   agg::span_allocator<color_type> sa;

   typedef agg::span_interpolator_linear<> interpolator_type;
   interpolator_type interpolator(img_mtx);

   typedef agg::image_accessor_clip<pixfmt_type> img_source_type;

   pixfmt_type img_pixf(app.rbuf_img(0));
   img_source_type img_src(img_pixf, agg::rgba_pre(0, 0.4, 0, 0.5));

   /*
   // Version without filtering (nearest neighbor)
   //------------------------------------------
   typedef agg::span_image_filter_rgb_nn<img_source_type,
   interpolator_type> span_gen_type;
   span_gen_type sg(img_src, interpolator);
   //------------------------------------------
   */

   // Version with "hardcoded" bilinear filter and without 
   // image_accessor (direct filter, the old variant)
   //------------------------------------------
   typedef agg::span_image_filter_rgb_bilinear_clip<pixfmt_type,
           interpolator_type> span_gen_type;
   span_gen_type sg(img_pixf, agg::rgba_pre(0, 0.4, 0, 0.5), interpolator);
   //------------------------------------------

   /*
   // Version with arbitrary 2x2 filter
   //------------------------------------------
   typedef agg::span_image_filter_rgb_2x2<img_source_type,
   interpolator_type> span_gen_type;
   agg::image_filter<agg::image_filter_kaiser> filter;
   span_gen_type sg(img_src, interpolator, filter);
   //------------------------------------------
   */
   /*
   // Version with arbitrary filter
   //------------------------------------------
   typedef agg::span_image_filter_rgb<img_source_type,
   interpolator_type> span_gen_type;
   agg::image_filter<agg::image_filter_spline36> filter;
   span_gen_type sg(img_src, interpolator, filter);
   //------------------------------------------
   */

   agg::rasterizer_scanline_aa<> ras;
   ras.clip_box(0, 0, width(), height());
   agg::scanline_u8 sl;
   double r = app.initial_width();
   if(app.initial_height() - 60 < r) r = app.initial_height() - 60;
   agg::ellipse ell(app.initial_width()  / 2.0 + 10, 
         app.initial_height() / 2.0 + 20 + 10, 
         r / 2.0 + 16.0, 
         r / 2.0 + 16.0, 200);


   agg::conv_transform<agg::ellipse> tr(ell, src_mtx);

   ras.add_path(tr);
   agg::render_scanlines_aa(ras, sl, rb_pre, sa, sg);

   agg::render_ctrl(ras, sl, rb, m_angle);
   agg::render_ctrl(ras, sl, rb, m_scale);
}
