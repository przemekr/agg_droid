#include "app_support.h"

struct atom_type
{
   double   x;
   double   y;
   char     label[4];
   int      charge;
   unsigned color_idx;
};

struct bond_type
{
   unsigned idx1;
   unsigned idx2;
   double   x1;
   double   y1;
   double   x2;
   double   y2;
   unsigned order;
   int      stereo;
   int      topology;
};


class molecule
{
public:
   ~molecule();
   molecule();

   bool read(FILE* fd);

   unsigned num_atoms() const { return m_num_atoms; }
   unsigned num_bonds() const { return m_num_bonds; }

   const atom_type& atom(unsigned idx) const { return m_atoms[idx]; }
   const bond_type& bond(unsigned idx) const { return m_bonds[idx]; }

   double average_bond_len() const { return m_avr_len; }

   const char* name() const { return m_name; }

   static int    get_int(const char* buf, int pos, int len);
   static double get_dbl(const char* buf, int pos, int len);
   static char*  get_str(char* dst, const char* buf, int pos, int len);

private:
   atom_type* m_atoms;
   unsigned   m_num_atoms;
   bond_type* m_bonds;
   unsigned   m_num_bonds;
   char       m_name[128];
   double     m_avr_len;
};


enum atom_color_e
{
   atom_color_general = 0,
   atom_color_N       = 1,
   atom_color_O       = 2,
   atom_color_S       = 3,
   atom_color_P       = 4,
   atom_color_halogen = 5,
   end_of_atom_colors
};



class Mol_view: public AppView
{   
    molecule*                    m_molecules;
    unsigned                     m_num_molecules;
    unsigned                     m_cur_molecule;
    agg::slider_ctrl<agg::rgba8> m_thickness;
    agg::slider_ctrl<agg::rgba8> m_text_size;
    double     m_pdx;
    double     m_pdy;
    double     m_center_x;
    double     m_center_y;
    double     m_scale;
    double     m_prev_scale;
    double     m_angle;
    double     m_prev_angle;
    bool       m_mouse_move;
    agg::rgba8 m_atom_colors[end_of_atom_colors];

public:
    virtual ~Mol_view();
    Mol_view(App& app);
    virtual void on_init();
    virtual void on_draw();
    virtual void on_idle();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_key(int, int, unsigned key, unsigned);
    virtual void on_ctrl_change();
private:
    agg::button_ctrl<agg::rgba8> next;
    agg::button_ctrl<agg::rgba8> left;
    agg::button_ctrl<agg::rgba8> right;
};
