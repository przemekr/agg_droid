#include "app_support.h"

class Image1: public AppView
{
    agg::slider_ctrl<agg::rgba8> m_angle;
    agg::slider_ctrl<agg::rgba8> m_scale;

public:
    Image1(App& app);
    virtual ~Image1();
    virtual void on_draw();
};
