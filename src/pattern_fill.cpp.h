#include "app_support.h"

class Pattern_fill: public AppView
{
    agg::slider_ctrl<agg::rgba> m_polygon_angle;
    agg::slider_ctrl<agg::rgba> m_polygon_scale;

    agg::slider_ctrl<agg::rgba> m_pattern_angle;
    agg::slider_ctrl<agg::rgba> m_pattern_size;

    agg::slider_ctrl<agg::rgba> m_pattern_alpha;

    agg::cbox_ctrl<agg::rgba> m_rotate_polygon;
    agg::cbox_ctrl<agg::rgba> m_rotate_pattern;
    agg::cbox_ctrl<agg::rgba> m_tie_pattern;

    double m_polygon_cx;
    double m_polygon_cy;

    double m_dx;
    double m_dy;

    int m_flag;

    agg::int8u*           m_pattern;
    agg::rendering_buffer m_pattern_rbuf;

    agg::rasterizer_scanline_aa<> m_ras;
    agg::scanline_p8 m_sl;
    agg::path_storage m_ps;


public:
    Pattern_fill(App& app);
    virtual ~Pattern_fill();
    void create_star(double xc, double yc,
                     double r1, double r2, 
                     unsigned n, double start_angle = 0.0);
    void generate_pattern();
    virtual void on_init();
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
    virtual void on_ctrl_change();
    virtual void on_idle();
};
