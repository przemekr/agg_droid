#include "app_support.h"

class Compositing2: public AppView
{
    agg::slider_ctrl<color_type>    m_alpha_dst;
    agg::slider_ctrl<color_type>    m_alpha_src;
    agg::rbox_ctrl<agg::rgba8>      m_comp_op;

    agg::pod_auto_array<color_type, 256> m_ramp1;
    agg::pod_auto_array<color_type, 256> m_ramp2;

    agg::rasterizer_scanline_aa<> m_ras;
    agg::scanline_u8 m_sl;

public:
    Compositing2(App&);
    template<class RenBase, class ColorRamp> 
    void radial_shape(RenBase& rbase, const ColorRamp& colors,
                      double x1, double y1, double x2, double y2);

    template<class RenBase> void render_scene(RenBase& rb);
    virtual void on_draw();
};
