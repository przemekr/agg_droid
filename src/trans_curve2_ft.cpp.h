#include "app_support.h"
#include "agg_font_freetype.h"
#include "interactive_polygon.h"

class Trans_curve2_ft: public AppView
{
public:
    typedef agg::renderer_base<pixfmt_type> renderer_base;
    typedef agg::renderer_scanline_aa_solid<renderer_base> renderer_solid;
    typedef agg::scanline_p8 scanline_type;
    typedef agg::font_engine_freetype_int16 font_engine_type;
    typedef agg::font_cache_manager<font_engine_type> font_manager_type;

    font_engine_type             m_feng;
    font_manager_type            m_fman;
    agg::interactive_polygon     m_poly1;
    agg::interactive_polygon     m_poly2;
    agg::slider_ctrl<agg::rgba8> m_num_points;
    agg::cbox_ctrl<agg::rgba8>   m_fixed_len;
    agg::cbox_ctrl<agg::rgba8>   m_preserve_x_scale;
    agg::cbox_ctrl<agg::rgba8>   m_animate;
    double                       m_dx1[6];
    double                       m_dy1[6];
    double                       m_dx2[6];
    double                       m_dy2[6];
    bool                         m_prev_animate;

    Trans_curve2_ft(App& app);
    virtual void on_init();
    virtual void on_draw();
    virtual void on_mouse_button_down(int x, int y, unsigned flags);
    virtual void on_mouse_move(int x, int y, unsigned flags);
    virtual void on_mouse_button_up(int x, int y, unsigned flags);
    virtual void on_key(int x, int y, unsigned key, unsigned flags);
    virtual void on_ctrl_change();
    void move_point(double& x, double& y, double& dx, double& dy);
    void normalize_point(unsigned i);
    virtual void on_idle();
};
