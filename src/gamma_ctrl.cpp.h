#include "app_support.h"

class Gamma_ctrl: public AppView
{
public:
    Gamma_ctrl(App& app);
    virtual void on_init();
    virtual ~Gamma_ctrl();
    virtual void on_draw();
};
