#include "app_support.h"
#include "ctrl/agg_spline_ctrl.h"

class Image_alpha: public AppView
{
    agg::spline_ctrl<agg::rgba8> m_alpha;
    double     m_x[50];
    double     m_y[50];
    double     m_rx[50];
    double     m_ry[50];
    agg::rgba8 m_colors[50];

public:
    Image_alpha(App& app);
    virtual ~Image_alpha();
    virtual void on_init();
    virtual void on_draw();
    virtual void on_key(int x, int y, unsigned key, unsigned flags);
};
