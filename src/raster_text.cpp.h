#include "app_support.h"
#include "agg_glyph_raster_bin.h"

class Raster_text: public AppView
{
public:
    typedef agg::pixfmt_bgr24 pixfmt;
    typedef agg::renderer_base<pixfmt> ren_base;
    typedef agg::glyph_raster_bin<agg::rgba8> glyph_gen;

    Raster_text(App& app);
    virtual void on_draw();
};
