/*
 * 24-game, a simple arithmetic game.
 * Copyright 2014 Przemyslaw Rzepecki
 * Contact: przemekr@sdfeu.org
 * 
 * This file is part of 24-game.
 * 
 * 24-game is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * 24-game is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * 24-game.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APP_SUPPORT__
#define APP_SUPPORT__

#include <math.h>
#include <algorithm> 
#include <stack>
#include <unistd.h>
#include "agg_basics.h"
#include "agg_rendering_buffer.h"
#include "agg_rasterizer_scanline_aa.h"
#include "agg_scanline_u.h"
#include "agg_scanline_p.h"
#include "agg_renderer_scanline.h"
#include "agg_ellipse.h"
#include "agg_pixfmt_rgb.h"
#include "agg_gamma_lut.h"
#include "agg_pixfmt_amask_adaptor.h"
#include "agg_span_allocator.h"
#include "agg_span_gradient.h"
#include "agg_span_interpolator_linear.h"
#include "agg_image_accessors.h"
#include "agg_renderer_outline_image.h"
#include "agg_renderer_outline_aa.h"
#include "agg_rasterizer_outline_aa.h"
#include "agg_conv_transform.h"
#include "agg_color_rgba.h"
#include "agg_pixfmt_rgba.h"
#include "agg_gsv_text.h"
#include "agg_button_ctrl.h"
#include "ctrl/agg_slider_ctrl.h"
#include "ctrl/agg_cbox_ctrl.h"
#include "ctrl/agg_rbox_ctrl.h"
#include "ctrl/agg_bezier_ctrl.h"
#include "ctrl/agg_polygon_ctrl.h"
#include "ctrl/agg_scale_ctrl.h"
#include "ctrl/agg_gamma_ctrl.h"
#include "platform/agg_platform_support.h"
#include <unistd.h>
#include <stdint.h>
#include "stdio.h"
#include "time.h"

#include <time.h>

#ifdef MOBILE
#define START_H  400
#define START_W  640
#define WINDOW_FLAGS agg::window_fullscreen | agg::window_keep_aspect_ratio
#else
#define START_H  400
#define START_W  640
#define WINDOW_FLAGS agg::window_resize | agg::window_hw_buffer
#endif

#define CTRL_TEXT_THICKNESS 3

enum flip_y_e { flip_y = true };
static const agg::rgba transp  (0.0, 0.0, 0.0, 0.0);
static const agg::rgba dblue   (0.0, 0.0, 0.8, 0.8);
static const agg::rgba lblue   (0.0, 0.0, 0.9, 1.0);
static const agg::rgba lgreen  (0,0.9,0,1);
static const agg::rgba lgray   (0.8,0.8,0.8);
static const agg::rgba green   (0,120,0);
static const agg::rgba yellow  (63,63,0);
static const agg::rgba red     (0.9,0,0,0.7);
static const agg::rgba black   (0,0,0);
typedef agg::rgba8             color_type;

typedef agg::gamma_lut<agg::int8u, agg::int8u, 8, 8>   gamma_lut_type;
typedef agg::pixfmt_bgr24_gamma<gamma_lut_type> pixfmt_gamma_type;
typedef agg::pixfmt_bgr24     pixfmt_type;
typedef agg::pixfmt_bgr24_pre pixfmt_pre;

typedef agg::renderer_base<pixfmt_type> renderer_base_type;
typedef agg::renderer_base<pixfmt_pre> renderer_base_pre;
typedef agg::renderer_scanline_aa_solid<renderer_base_type> renderer_scanline_type;
typedef agg::rasterizer_scanline_aa<> rasterizer_scanline;
typedef agg::scanline_u8 scanline;


class View
{
public:
   virtual void enter() {}
   virtual void on_draw() {}
   virtual void on_resize(int sx, int sy) {}
   virtual void on_idle() {}
   virtual void on_init() {}
   virtual void on_mouse_move(int x, int y, unsigned flags) {}
   virtual void on_multi_gesture(float dTheta, float dDist, float x,
         float y, int numFinders) {}
   virtual void on_touch_event(float x, float y, float dx, float dy, int id, bool down) {}
   virtual void on_mouse_button_down(int x, int y, unsigned flags) {}
   virtual void on_mouse_button_up(int x, int y, unsigned flags) {}
   virtual void on_key(int x, int y, unsigned key, unsigned flags) {}
   virtual void on_ctrl_change() {}
   agg::ctrl_container m_ctrls;
   void add_ctrl(agg::ctrl& c) { m_ctrls.add(c); }
};


class App: public agg::platform_support
{
public:
   App(agg::pix_format_e format, bool flip_y) :
      agg::platform_support(format, flip_y),
      sound(false), w(0), h(0)
   {
   }

   virtual void changeView(const char* name) {};

   bool sound;

   void draw_text(double x, double y, double size, const char* str)
   {
      agg::rgba c(0.8, 0, 0, 0.6);
      draw_text(x, y, size, c, 1.0, str);
   }

   void draw_text(double x, double y, double size, agg::rgba& c, double tickness, const char* str)
   {
      agg::rasterizer_scanline_aa<> m_ras;
      agg::scanline_p8              m_sl;
      pixfmt_type pf(rbuf_window());
      agg::renderer_base<pixfmt_type> rb(pf);
      agg::renderer_scanline_aa_solid<agg::renderer_base<pixfmt_type> > ren(rb);

      agg::gsv_text txt;
      agg::conv_stroke<agg::gsv_text> txt_stroke(txt);
      txt_stroke.width(size/6*tickness);
      txt_stroke.line_cap(agg::round_cap);
      txt.size(size);
      txt.start_point(x, y);
      txt.text(str);
      m_ras.add_path(txt_stroke);
      ren.color(c);
      agg::render_scanlines(m_ras, m_sl, ren);
   }

   virtual void on_ctrl_change()
   {
      view->on_ctrl_change();
   }

   virtual void on_resize(int nx, int ny)
   {
      w = nx; h = ny;
      view->on_resize(nx, ny);
   }

   virtual void on_idle()
   {
      view->on_idle();
   }

   virtual void on_mouse_button_up(int x, int y, unsigned flags)
   {
      view->on_mouse_button_up(x, y, flags);
   }

   virtual void on_mouse_button_down(int x, int y, unsigned flags)
   {
      view->on_mouse_button_down(x, y, flags);
   }

   virtual void on_mouse_move(int x, int y, unsigned flags)
   {
      view->on_mouse_move(x, y, flags);
   }

   virtual void on_multi_gesture(float x, float y,
         float dTheta, float dDist, int numFingers)
   {
      view->on_multi_gesture(x, y, dTheta, dDist, numFingers);
   }
   virtual void on_touch_event(float x, float y, float dx, float dy,
         int id, bool down)
   {
      view->on_touch_event(x, y, dx, dy, id, down);
   }

   virtual void on_draw()
   {
      view->on_draw();
   }

   void on_key(int x, int y, unsigned key, unsigned flags)
   {
       ERROR_PRINT("on key in app support %u", key);
       
      if (key == 27)
         changeView("list");

      if (key == 0x4000010e) /* Android BACK key*/
         changeView("list");

      view->on_key(x, y, key, flags);
   }


protected:
   View* view;
   int w, h;
};

class AppView: public View
{
protected:
   AppView(App& application): app(application) {}
   int width()          { return app.width(); }
   int height()         { return app.height(); }
   void start_timer()   { app.start_timer(); }
   long elapsed_time()  { return app.elapsed_time(); }
   void update_window() { app.update_window(); }
   void force_redraw()  { app.force_redraw(); }
   bool wait_mode()     { return app.wait_mode(); }
   void wait_mode(bool v) {  app.wait_mode(v); }
   agg::rendering_buffer& rbuf_window() { return app.rbuf_window(); }
   void message(const char* buf)   { app.message(buf); }
   App& app;
};

/* IMAGES INDEXES */
#define IMG_AGG            (0)
#define IMG_COMPOSITING    (1)
#define IMG_SPHERES        (2)
#define IMG_LINE_PATTERN_0 (3)
#define IMG_LINE_PATTERN_1 (4)
#define IMG_LINE_PATTERN_2 (5)
#define IMG_LINE_PATTERN_3 (6)
#define IMG_LINE_PATTERN_4 (7)
#define IMG_LINE_PATTERN_5 (8)
#define IMG_LINE_PATTERN_6 (9)
#define IMG_LINE_PATTERN_7 (10)
#define IMG_LINE_PATTERN_8 (11)
#define IMG_TMP1 (12)









#endif /* end of: APP_SUPPORT__ */
