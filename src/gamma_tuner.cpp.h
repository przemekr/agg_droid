#include "app_support.h"

class Gamma_tuner: public AppView
{
    agg::slider_ctrl<agg::rgba8> m_gamma;
    agg::slider_ctrl<agg::rgba8> m_r;
    agg::slider_ctrl<agg::rgba8> m_g;
    agg::slider_ctrl<agg::rgba8> m_b;
    agg::rbox_ctrl  <agg::rgba8> m_pattern;

public:
    Gamma_tuner(App& app);
    virtual void on_draw();
};
