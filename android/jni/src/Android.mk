LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := main

SDL_PATH := ../SDL

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include $(LOCAL_PATH)/agg-2.5/include $(LOCAL_PATH)/agg-2.5/font_freetype/
LOCAL_CPPFLAGS += -DMOBILE -DDEBUG
LOCAL_CXXFLAGS += -fexceptions

# Add your application source files here...
LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.c \
	agg-2.5/src/agg_arc.cpp \
	agg-2.5/src/agg_arrowhead.cpp \
	agg-2.5/src/agg_bezier_arc.cpp \
	agg-2.5/src/agg_bspline.cpp \
	agg-2.5/src/agg_curves.cpp \
	agg-2.5/src/agg_embedded_raster_fonts.cpp \
	agg-2.5/src/agg_gsv_text.cpp \
	agg-2.5/src/agg_image_filters.cpp \
	agg-2.5/src/agg_line_aa_basics.cpp \
	agg-2.5/src/agg_line_profile_aa.cpp \
	agg-2.5/src/agg_rounded_rect.cpp \
	agg-2.5/src/agg_sqrt_tables.cpp \
	agg-2.5/src/agg_trans_affine.cpp \
	agg-2.5/src/agg_trans_double_path.cpp \
	agg-2.5/src/agg_trans_single_path.cpp \
	agg-2.5/src/agg_trans_warp_magnifier.cpp \
	agg-2.5/src/agg_vcgen_bspline.cpp \
	agg-2.5/src/agg_vcgen_contour.cpp \
	agg-2.5/src/agg_vcgen_dash.cpp \
	agg-2.5/src/agg_vcgen_markers_term.cpp \
	agg-2.5/src/agg_vcgen_smooth_poly1.cpp \
	agg-2.5/src/agg_vcgen_stroke.cpp \
	agg-2.5/src/agg_vpgen_clip_polygon.cpp \
	agg-2.5/src/agg_vpgen_clip_polyline.cpp \
	agg-2.5/src/agg_vpgen_segmentator.cpp \
	agg-2.5/src/ctrl/agg_bezier_ctrl.cpp \
	agg-2.5/src/ctrl/agg_cbox_ctrl.cpp \
	agg-2.5/src/ctrl/agg_gamma_ctrl.cpp \
	agg-2.5/src/ctrl/agg_gamma_spline.cpp \
	agg-2.5/src/ctrl/agg_polygon_ctrl.cpp \
	agg-2.5/src/ctrl/agg_rbox_ctrl.cpp \
	agg-2.5/src/ctrl/agg_scale_ctrl.cpp \
	agg-2.5/src/ctrl/agg_slider_ctrl.cpp \
	agg-2.5/src/ctrl/agg_spline_ctrl.cpp \
	agg-2.5/src/platform/sdl2/agg_platform_support.cpp \
	agg-2.5/font_freetype/agg_font_freetype.cpp \
	agg_app.cc agg_button_ctrl.cpp \
	aa_demo.cpp \
	aa_test.cpp \
	alpha_gradient.cpp \
	alpha_mask2.cpp \
	alpha_mask3.cpp \
	alpha_mask.cpp \
	bezier_div.cpp \
	blend_color.cpp \
	blur.cpp \
	bspline.cpp \
	circles.cpp \
	component_rendering.cpp \
	compositing2.cpp \
	compositing.cpp \
	conv_contour.cpp \
	conv_dash_marker.cpp \
	conv_stroke.cpp \
	distortions.cpp \
	flash_rasterizer.cpp \
	gamma_correction.cpp \
	gamma_ctrl.cpp \
	gamma_tuner.cpp \
	gouraud.cpp \
	gouraud_mesh.cpp \
	gpc.c \
	gpc_test.cpp \
	gradient_focal.cpp \
	gradients.cpp \
	graph_test.cpp \
	idea.cpp \
	image1.cpp \
	image_alpha.cpp \
	image_filters2.cpp \
	image_filters.cpp \
	image_fltr_graph.cpp \
	image_perspective.cpp \
	image_resample.cpp \
	image_transforms.cpp \
	interactive_polygon.cpp \
	line_patterns_clip.cpp \
	line_patterns.cpp \
	lion.cpp \
	lion_lens.cpp \
	lion_outline.cpp \
	make_arrows.cpp \
	make_gb_poly.cpp \
	mol_view.cpp \
	multi_clip.cpp \
	parse_lion.cpp \
	pattern_fill.cpp \
	pattern_perspective.cpp \
	pattern_resample.cpp \
	rasterizer_compound.cpp \
	raster_text.cpp \
	rounded_rect.cpp \
	scanline_boolean2.cpp \
	scanline_boolean.cpp \
	simple_blur.cpp \
	trans_curve1_ft.cpp \
	trans_curve2_ft.cpp \
	trans_polar.cpp

LOCAL_SHARED_LIBRARIES := SDL2 SDL2_image SDL2_mixer

LOCAL_LDLIBS := -lGLESv1_CM -llog
LOCAL_STATIC_LIBRARIES := freetype

include $(BUILD_SHARED_LIBRARY)
